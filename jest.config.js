const common = require("./config/test/jest.common");

process.env.BABEL_ENV = "test";
process.env.NODE_ENV = "test";
process.env.PUBLIC_URL = "";

module.exports = {
  ...common,
  roots: null,
  coverageDirectory: "<rootDir>/.coverage/unit",
  collectCoverageFrom: [
    "<rootDir>/packages/*/src/**/*.{ts,tsx}",
    "!<rootDir>/packages/*/src/generated.{ts,tsx}",
    "!<rootDir>/packages/*/src/tests/*",
    "!<rootDir>/packages/*/src/**/*.d.ts",
    "!<rootDir>/packages/*/src/**/*.stories.*",
    "!<rootDir>/packages/*/src/**/types/*",
    "!<rootDir>/packages/*/src/**/*.types.*",
    "!<rootDir>/packages/*/src/**/*.cy.spec.*",
  ],
  coverageReporters: ["json", "lcov", "text", "clover"],
  coverageThreshold: {
    global: {
      statements: 80,
      branches: 70,
      functions: 80,
      lines: 80,
    },
  },
  testURL: "http://localhost/",
  projects: ["<rootDir>/packages/*/jest.config.js"],
};
