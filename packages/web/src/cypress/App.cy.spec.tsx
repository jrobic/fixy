import { mount } from "@cypress/react";
import Color from "color";
import App from "../App";

it("renders learn react link", () => {
  // expect(true).equal(true);
  mount(<App />);

  cy.findByText(/learn react/i);

  cy.findByText(/src\/App.tsx/)
    .should("have.css", "color", Color("cyan").toString())
    .click()
    .should("not.have.css", "color", Color("cyan").toString());

  cy.findByText(/src\/App.tsx/).then(($code) => {
    const color = $code.css("color");

    cy.findByText(/src\/App.tsx/)
      .click()
      .should("not.have.css", "color", color);
  });
});
