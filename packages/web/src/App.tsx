/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import { sayHello } from "@fixy/shared";

import { useState } from "react";
import logo from "./logo.svg";
import "./App.css";

// eslint-disable-next-line no-bitwise
const randomColor = () => `#${(((1 << 24) * Math.random()) | 0).toString(16)}`;

function App() {
  sayHello("CRA");

  const [color, setColor] = useState("cyan");

  return (
    <div className="App">
      <header className="App-header">
        <h1>
          <img src={logo} className="App-logo" alt="logo" />
        </h1>
        <p>
          Edit{" "}
          <code style={{ color }} onClick={() => setColor(randomColor())}>
            src/App.tsx
          </code>{" "}
          and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
