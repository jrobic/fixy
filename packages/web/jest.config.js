const packageName = require("./package.json").name.replace(/@\w+\//, "");
const client = require("../../config/test/jest.client")(packageName);

module.exports = {
  ...client,
  testPathIgnorePatterns: ["cypress"],
  setupFiles: ["react-app-polyfill/jsdom"],
  transform: {
    ...client.transform,
    "^.+\\.(js|jsx)$": `react-scripts/config/jest/babelTransform.js`,
    "^.+\\.css$": `react-scripts/config/jest/cssTransform.js`,
    "^(?!.*\\.(js|jsx|ts|tsx|css|json)$)": `react-scripts/config/jest/fileTransform.js`,
  },
  moduleNameMapper: {
    ...client.moduleNameMapper,
    "^react-native$": "react-native-web",
    "^.+\\.module\\.(css|sass|scss)$": "identity-obj-proxy",
  },
  moduleFileExtensions: [
    "web.js",
    "js",
    "web.ts",
    "ts",
    "web.tsx",
    "tsx",
    "json",
    "web.jsx",
    "jsx",
    "node",
  ],
};
