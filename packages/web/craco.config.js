const webpackAddTSPaths = require("../../config/webpack/webpackAddTSPaths");

process.env.SKIP_PREFLIGHT_CHECK = true;
process.env.ESLINT_EXTEND = true;

module.exports = {
  webpack: {
    configure: webpackAddTSPaths,
  },
};
