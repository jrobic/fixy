/* eslint-disable no-console */
/* eslint-disable global-require */
/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const isCI = require("is-ci");
const { lighthouse, pa11y, prepareAudit } = require("cypress-audit");
const { startDevServer } = require("@cypress/webpack-dev-server");

const findReactScriptsWebpackConfig = require("@cypress/react/plugins/react-scripts/findReactScriptsWebpackConfig");
const webpackAddTSPaths = require("../../../../config/webpack/webpackAddTSPaths");
/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
  require("@cypress/code-coverage/task")(on, config);

  // @see @cypress/react/plugins/react-scripts/index.js
  on("dev-server:start", (options) => {
    return startDevServer({
      options,
      webpackConfig: webpackAddTSPaths(findReactScriptsWebpackConfig(config)),
    });
  });
  // eslint-disable-next-line no-param-reassign
  config.env.reactDevtools = true;

  const isProd = process.env.NODE_ENV === "production";

  if (!isCI) {
    // eslint-disable-next-line no-param-reassign
    config.baseUrl = !isProd ? "http://localhost:8000" : "http://localhost:9001";
  }

  on("before:browser:launch", (_, launchOptions) => {
    prepareAudit(launchOptions);
  });

  on("task", {
    lighthouse: lighthouse((lighthouseReport) => {
      console.log(lighthouseReport); // raw lighthouse reports
    }),
    pa11y: pa11y((pa11yReport) => {
      console.log(pa11yReport); // raw pa11y reports
    }),
  });

  return config;
};
