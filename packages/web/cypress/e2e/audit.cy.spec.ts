describe("audit", () => {
  it("should audit", () => {
    cy.visit("/");
    cy.lighthouse({
      performance: 85,
      accessibility: 50,
      "best-practices": 85,
      seo: 85,
      pwa: 50,
    });
    cy.pa11y();
  });
});
