const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin");

process.env.SKIP_PREFLIGHT_CHECK = true;
process.env.ESLINT_EXTEND = true;

const isProd = process.env.NODE_ENV === "production";

module.exports = {
  stories: ["../src/**/*.stories.mdx", "../src/**/*.stories.@(js|jsx|ts|tsx)"],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-a11y",
    "@storybook/preset-create-react-app",
  ],
  webpackFinal: (webpackConfig) => {
    webpackConfig.node = {
      fs: "empty",
      tls: "empty",
      net: "empty",
      module: "empty",
      console: true,
    };
    console.log("isProd", isProd);
    if (isProd) {
      return webpackConfig;
    }

    webpackConfig.resolve.plugins = webpackConfig.resolve.plugins.filter(
      (plugin) => plugin.constructor.name !== "ModuleScopePlugin"
    );

    // Remove the ModuleScopePlugin which throws when we try to import something
    // outside of src/.
    webpackConfig.resolve.plugins.pop();

    // Resolve the path aliases.
    webpackConfig.resolve.plugins.push(new TsconfigPathsPlugin());

    // Let Babel compile outside of src/.
    const oneOfRule = webpackConfig.module.rules.find((rule) => rule.oneOf);
    const tsRule = oneOfRule.oneOf.find((rule) =>
      rule.test.toString().includes("ts|tsx")
    );
    tsRule.include = undefined;
    tsRule.exclude = /node_modules/;

    return webpackConfig;
  },
};
