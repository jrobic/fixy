// eslint-disable-next-line import/no-extraneous-dependencies
import { createTestClient, ApolloServerTestClient } from "apollo-server-testing";
import { startServer } from "../server";

require("@fixy/shared/utils/env");

type CreateServer = ApolloServerTestClient & { close(): Promise<void> };

export async function createServer(): Promise<CreateServer> {
  const { server } = await startServer();
  const testClient = createTestClient(server);

  async function close(): Promise<void> {
    await server.stop();
  }

  return { ...testClient, close };
}
