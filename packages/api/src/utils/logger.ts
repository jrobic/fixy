import morgan from "morgan";

/* istanbul ignore next */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
morgan.token("graphql", (req: any): string => {
  const { operationName } = req.body || { operationName: null };

  if (operationName) {
    const regex = new RegExp(`(\\w*)[ ]*${operationName}`);
    const operationType = req.body.query.match(regex)[1];
    return `${operationType}:${operationName}`;
  }
  return "";
});

/* istanbul ignore next */
export function graphqlLogger() {
  return morgan(":method :url :graphql :status :response-time ms - :res[content-length]", {
    skip: (req: any): boolean => {
      const { operationName } = req.body || {
        operationName: null,
      };

      if (operationName === "IntrospectionQuery") {
        return true;
      }
      return false;
    },
  });
}
