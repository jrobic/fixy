import express from "express";
import { ApolloServer } from "apollo-server-express";
import { altairExpress } from "altair-express-middleware";

import { StartServerResponse } from "./types/server";
import { ContextFn } from "./types/context";
import { executableSchema } from "./createSchemas";
import { graphqlLogger } from "./utils/logger";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const { getEnv } = require("@fixy/shared");

export const startServer = async (): Promise<StartServerResponse> => {
  const app = express();
  const config = getEnv();
  const isDev = config.NODE_ENV !== "production";

  /* istanbul ignore next */
  if (process.env.NODE_ENV !== "test") {
    app.use(graphqlLogger());
  }

  const context: ContextFn = ({ req, res }) => ({
    req,
    res,
    env: process.env.NODE_ENV || "development",
    config,
    loaders: {},
  });

  if (isDev) {
    // Mount your Altair GraphQL client
    app.use(
      "/altair",
      altairExpress({
        endpointURL: "/graphql",
      }),
    );
  }

  const server = new ApolloServer({
    schema: executableSchema,
    context,
    playground: isDev && {
      settings: {
        "request.credentials": "include",
      },
    },
    tracing: isDev,
    introspection: isDev,
  });

  server.applyMiddleware({
    app,
    cors: {
      origin: "*",
      credentials: true,
    },
    // path: "/graphql",
  });

  return {
    app,
    server,
  };
};
