/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { DateTimeResolver, JSONObjectResolver } from "graphql-scalars";
import { Context } from "../../types/context";
import { Resolvers } from "../../types/generated";
import { name, version } from "../../utils/version";

const commonResolvers: Resolvers<Context> = {
  JSONObject: JSONObjectResolver,
  DateTime: DateTimeResolver,
  Query: {
    version(parent, args, ctx) {
      return {
        name,
        env: ctx.env,
        sha1: "",
        date: new Date(),
        version,
      };
    },
    async status() {
      const res = {
        uptime: Math.round(process.uptime()),
        timestamp: Date.now(),
        message: "ok",
      };

      return res;
    },
  },
};

export default commonResolvers;
