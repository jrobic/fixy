/* istanbul ignore file */
// eslint-disable-next-line @typescript-eslint/no-var-requires
import { startServer } from "./server";
import { StartServerResponse } from "./types/server";

const { getEnv } = require("@fixy/shared");

const config = getEnv();

startServer().then(({ app, server }: StartServerResponse): void => {
  app.listen(config.FIXY_API_PORT, () => {
    // eslint-disable-next-line no-console
    console.log(`Server started at http://localhost:${config.FIXY_API_PORT}${server.graphqlPath}`);
  });
});
