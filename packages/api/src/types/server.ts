import { Express } from "express";
import { ApolloServer } from "apollo-server-express";

export interface StartServerResponse {
  app: Express;
  server: ApolloServer;
}
