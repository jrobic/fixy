import { ExpressContext } from "apollo-server-express/dist/ApolloServer";
import { ContextFunction } from "apollo-server-core";
import { Config } from "./config";

export type Context = {
  env: string;
  config: Config;
  loaders: {
    // siteLoader: ReturnType<typeof siteLoader>;
  };
} & ExpressContext;

export type ContextFn = ContextFunction<ExpressContext, Context>;
