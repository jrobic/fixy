export type Config = {
  FIXY_API_PORT: string;
  [k: string]: string;
};
