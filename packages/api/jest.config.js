const packageName = require("./package.json").name.replace(/@\w+\//, "");
const server = require("../../config/test/jest.server")(packageName);

module.exports = {
  ...server,
};
