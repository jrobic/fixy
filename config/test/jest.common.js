/* eslint-disable import/no-extraneous-dependencies */
const path = require("path");
const { pathsToModuleNameMapper } = require("ts-jest/utils");
const { compilerOptions } = require("../../tsconfig.json");

module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  rootDir: path.join(__dirname, "../.."),
  resetMocks: true,
  transformIgnorePatterns: ["[/\\\\]node_modules[/\\\\].+\\.tsx?$"],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
  },
  moduleDirectories: ["node_modules"],

  watchPlugins: [
    "jest-watch-typeahead/filename",
    "jest-watch-typeahead/testname",
    "jest-watch-select-projects",
  ],

  moduleNameMapper: pathsToModuleNameMapper(
    {
      ...compilerOptions.paths,
    },
    { prefix: "<rootDir>" },
  ),

  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.test.json",
      isolatedModules: true,
    },
  },
};
