/* eslint-disable import/no-extraneous-dependencies */
/**
 * This script merges the coverage reports from Cypress and Jest into a single one,
 * inside the "coverage" folder
 */
const { execSync } = require("child_process");
const fs = require("fs-extra");

const REPORTS_FOLDER = "reports";
const FINAL_OUTPUT_FOLDER = "coverage";

const run = (commands) => {
  commands.forEach((command) => execSync(command, { stdio: "inherit" }));
};

// Create the reports folder and move the reports from cypress and jest inside it
fs.emptyDirSync(REPORTS_FOLDER);

// fs.copyFileSync("./.coverage/e2e/coverage-final.json", `${REPORTS_FOLDER}/0-e2e.json`);

fs.copyFileSync("./.coverage/unit/coverage-final.json", `${REPORTS_FOLDER}/1-unit.json`);

fs.emptyDirSync(".nyc_output");
fs.emptyDirSync(FINAL_OUTPUT_FOLDER);

// Run "nyc merge" inside the reports folder, merging the two coverage files into one,
// then generate the final report on the coverage folder
run([
  // "nyc merge" will create a "coverage.json" file on the root, we move it to .nyc_output
  `./node_modules/.bin/nyc merge ${REPORTS_FOLDER} && mv coverage.json .nyc_output/out.json`,
  `./node_modules/.bin/nyc report --reporter lcov --report clover --reporter json --report-dir ${FINAL_OUTPUT_FOLDER}`,
  // `rimraf ${REPORTS_FOLDER}`,
]);

fs.removeSync(REPORTS_FOLDER);
